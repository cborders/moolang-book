# Data Types

|  Type   | Description |
|---------|-------------|
| int8    |             |
| int16   |             |
| int32   |             |
| int64   |             |
| uint8   |             |
| uint16  |             |
| uint32  |             |
| uint64  |             |
| float32 |             |
| float64 |             |
| string  |             |

## Casting
Moo does not to any automatic type coercion so moving from one type to another
requires an explicit cast. Casting is done using the `as` operator between the
value to be cast and the type is should be cast into.

```Moo
let myInt32 = 42
let myUint8 = myInt32 as uint8
```
