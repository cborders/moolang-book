# Map[K:V]
Maps are an unordered, resizable collection of key-value pairs. Just like the
rest of the containers in Moo, maps are strictly typed so the keys and values
must be of the same type for each entry.

## Methods
| Method | Description |
|--------|-------------|
| `containsKey(value : K) : bool` | Returns true of the map contains the given key. |

## Members
| Member | Description |
|--------|-------------|
| `size : int32` | Represents the number of key-value pairs in the map. |
| `keys : [K]` | A list containing all of the keys in the map. |

## Operators
| Operator | Description |
|----------|-------------|
| `[value : K] : V` | Returns the value associated with the given key. |
