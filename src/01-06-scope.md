# Scope
Just like access modifiers, scope declarations are stateful so anything after a
declaration is considered to be in that scope until there is another scope
declaration or the end if that file is reached. There are three scopes
`@global`, `@file`, and `@<named_scope>`.

| Scope | Description |
|-------|-------------|
| `@global` | Anything declared in global scope can be accessed anywhere else within the application. |
| `@file` | File scope restricts access to only the file in which things are declared. |
| `@<named_scope>` | Named scopes restrict access to within that scope. Using the same named scope in multiple locations will cause those bits of code to be considered in the same scope even if they are spread across multiple files. |

The default scope is file scope.
<div style="text-align:center">
<img src="./images/Scopes.png")
</div>
