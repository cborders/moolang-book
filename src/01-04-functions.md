# Functions
Functions are prefaced by the `fun` keyword. They can take in 0 or many 
rguments separated by commas. The parameters are made up of an identifier and a
type separated by a colon. They don't use the `var` or `let` keywords because
function arguments are always immutable.

```Moo
fun printTime()
{
    print(getTime() as string)
}

fun printNum(value : int32)
{
    print(value as string)
}

fun printError(line : int32, message : string)
{
    print("Error: [line \(line)] - " + message)
}
```

Functions can also return 0 to many values. The return types are listed
following the function declaration and are a list of types separated by commas.

```Moo
fun stringify(value : int32) : string
{
    return value as string
}

fun getExtremes(values : [int32]) : int32, int32
{
    var min = 0
    var max = 0

    for (value in values)
    {
        when
        {
            value < min : min = value
            value > max : max = value
        }
    }

    return min, max
}
```

Functions are first class types and can be used anywhere an expression can be
used. The type of a function is defined by a list of types separated by commas
and surrounded by parenthesis to represent the input parameters if there are
any. This is followed by a colon and a list of types separated by commas
representing the return types if there are any.

```Moo
var noArgFunction : ()
var singleArgFunction : (int32)
var multiArgFunction : (int32, int32)
var singleReturnFunction : () : int32
var multiReturnFunction : () : int32, int32
```

Functions can also be used as parameters to and returned from other functions.
The definition of type are exactly the same as above. Here is an example of a
function that takes a function as an argument. The function parameter in this
example doesn't take any parameters and doesn't return anything.

```Moo
fun callbackCaller(callback : ())
{
    callback()
}

fun myCallback()
{
    print("I'm a callback!")
}

callbackCaller(myCallback)

callbackCaller(() { print("I'm an anonymous function!") })
```

You can see that you can also define anonymous functions. These follow the same
syntax as named functions except that you leave off the fun keyword and the
name.

This is an example of a function taking a function as an argument. The function
parameter takes an argument.

```Moo
fun callbackCaller(callback : (string))
{
    callback("This is passed to the callback.")
}

fun myCallback(message : string)
{
    print(message)
}

callbackCaller(myCallback)

callbackCaller((message : string) { print(message) })
```

The following example is a function parameter that takes in a single argument
and returns an value.

```Moo
fun callbackCaller(callback : (int32) : string)
{
    print(callback(42))
}

fun myCallback(value : int32) : string
{
    return "The value passed in was \(value)"
}

callbackCaller(myCallback)

callbackCaller((value : int32) : string { return "Anonymous version of \(value)" })
```
