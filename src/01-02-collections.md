# Collections
## Lists
Lists in Moo are ordered, resizable collections. Every element in a list must be
of the same type and are indexed using `int32` values. Lists are declared using
the type that it will contain surrounded by square brackets `[]`.

```Moo
var names : [string]
```

Just like other types, list variable types can be inferred if the variable is
defined at the time of declaration. List literals are declared with square
brackets containing a comma separated list of expressions.

```Moo
var names = [ "Alice", "Bob", "Charlotte", "David", ]
```

Note the trailing comma in the list literal above. Trailing commas are allowed
in Moo and will not cause a compiler error.

Values are retrieved from a list using the index of the desired value surrounded
by square brackets.

```Moo
let name = names[0]
print(name)
```
<sub>*Output*</sub>
```
Alice
```

Lists also track some metadata about the list itself such as how many elements
it contains.

```Moo
let count = names.length
```

## Map

Maps are an unordered, resizable collection of key-value pairs. Just like the
rest of the containers in Moo, maps are strictly typed so the keys and values
must be of the same type for each entry. Maps are declared using square brackets
surround the key and value types separated by a colon.

```Moo
var childAges : [string : uint8]
```

Again, the type can be inferred when the variable is defined at the declaration.
Maps literals are defined using a comma separated list of key-value pairs
separated by a colon.

```Moo
var childAges = [
    "Aaron" : 7,
    "Barbara" : 9,
    "Charlie" : 4,
    "Debbie" : 5,
]
```

Values are retrieved by placing a key in square brackets.

```Moo
let age = childAges["Aaron"]
print(age as string)
```

<sub>*Output*</sub>
```
7
```
