# Build System
The standard library contains the compiler so you can create a build file using
the same language as the rest of your project. By default the command line
compiler will compile and run a file so if you pass it your build file it will
compile and run that file, thereby building your entire project. A benefit of
this, aside from having the build file in the same language as the rest of the
project is that your compiled build file is a native application and can just be
run again to build your project.

```Moo
let files = [
    "src/Main.moo",
    "src/Util.moo",
    "src/Bindings.moo",
]

let outputFileName = "build/MyApp"

let compiler = Compiler()

let app = compiler.build(files, outputFileName)

app.execute()
```
