# Basics

## Comments
In Moo comments work very similarly to other languages that you might be
familiar with. You can create a single line comment using `//` or comment blocks
by wrapping the comment in `/* */`.

```Moo
// This is a comment
/*
    Comments can be spread
    across multiple lines
*/
/*
    You can event /* nest */ multiline comments
*/
```

## Variables and Constants
Moo is a statically typed language and the type of variables must be known at
compile time. It is not necessary for the types to be declared explicitly if the
compiler can infer the type from context. For example, the following
declarations omit the variable type because they are initialized with values
whose type inferred by the compiler.

```Moo
var implicitInt = 42
var implicitFloat = 42.0
```

The default types for number literals are `int32` or `float32` depending on
whether the literal has a decimal part or not. If you want to override this
default type you can explicitly declare a type for the variable that you are
assigning to. Explicit types are declared by following the variable name with a
colon and then the desired type.

```Moo
var explicitInt : int8 = 42
var explicitFloat : float64 = 42.0
```

This is similar to the syntax that you use to declare a variable with out
immediately assigning it a value. This is required because the compiler has
no way to infer the data type.

```Moo
var notInitialized : uint16
```

Constants follow all of the same rules as variables except that they must be
initialized immediately and cannot be reassigned. Constants are signified with
the `let` keyword.

```Moo
let myConstant = 42
```

The previous code creates a constant of type `int32` with the value of 42. Just
like with variables we can change the implicit data type by declaring an
explicit type such as in the following code:

```Moo
let myConstantFloat : float32 = 42
```

## Strings

String literals are declared using double quotes.

```Moo
let message = "Hello World!"
```

Strings can be concatenated using the `+` operator.

```Moo
let name = "Alice"
let message = "Hello, " + name
```

Moo does not automatically coerce types so both sides of the concatenation
operator must be string types. This means that the programmer would need to
explicitly cast other types to string before passing it to the concatenation
operator.

```Moo
let score = 42
let message = "Your score is " + score as string
```

A better option would be to use string interpolation. This allows programmers to
build up strings from parts with a much cleaner syntax. Just declare a string
literal like you would normally and insert variables into it by wrapping them
with the `\( )` operator.

```Moo
let score = 42
let message = "Your score is \(score)"
```
