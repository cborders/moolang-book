# List[T]
Lists in Moo are ordered, resizable collections. Every element in a list must be
of the same type and are indexed using `int32` values.

## Methods
| Method | Description |
|--------|-------------|
|`contains(value : T) : bool` | Returns true if the value is found in the list. |
|`sort()` | Sorts the list in place based on the inherent order of the contents. |
|`sort((a : T, b : T) : int8)` | This version of sort takes a function that takes two items and returns -1 if the a is less than b, 0 if they are equal, or 1 if the a is greater than b. |
|`append(value : T)` | Adds an item to the end of the list. |
|`insert(value : T, index : int32)` | Adds an item to the list at the given index. |
|`indexOf(value : T) : int32` | Returns the first index of the given item if it is found or -1 if it is not in the list. |
|`lastIndexOf(value : T) : int32` | Returns the first index of the given item if it is found or -1 if it is not in the list. |

## Members
| Member | Description |
|--------|-------------|
| `size : int32` | Represents the number of items in the list. |

## Operators
| Operator | Description |
|----------|-------------|
| `[index : int32] : T` | Returns the item at the given index. |
