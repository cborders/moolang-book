# String

## Methods
| Method | Description |
|--------|-------------|
| `indexOf(substring : string) : int32` | Returns the first starting index of the given substring if it is found. If the substring is not found it returns -1. |
| `indexOf(char : uint8) : int32` | Returns the first index of the given character if it is found. If the character is not found it returns -1. |
| `lastIndexOf(substring : string) : int32` | Returns the last starting index of the given substring if it is found. If the substring is not found it returns -1. |
| `lastIndexOf(char : uint8) : int32` | Returns the last index of the given character if it is found. If the character is not found it returns -1. |

## Members
| Member | Description |
|--------|-------------|
| `length` | Represents the number of characters in the string. |

## Operators
| Operator | Description |
|----------|-------------|
| `[index : int32] : uint8` | Returns the character at the given index. |
| `[indices : range] : string` | Returns a substring starting matching the given range. |
