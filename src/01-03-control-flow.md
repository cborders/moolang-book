# Control Flow

## When
Moo has a single conditional execution keyword, `when`. `when` is used in places
where you would use `if`, `else if`, and `switch` statements in other languages.
In the simplest case `when` is followed by a boolean expression and a statement
separated by a colon.

```Moo
when <boolean_expression> { <statements> }
```

This means that when the boolean expression evaluates to true the statement is
run. This expression **must** produce a bool value. Consider an example where
you wanted make sure incoming values were positive so you clamped them to zero
if they were negative.

```Moo
when value < 0 { value = 0 }
```

Moo also has the `else` keyword to allow a statement to be run if the boolean
expression evaluates to false.

```Moo
when <boolean_expression>  { <true_statements> }
else { <false_statement> }
```

For this example let's say that you want to label a score of 50 or less as *low*
and anything else as *high*.

```Moo
var label : string
let score = 99

when score < 50 { label = "low" }
else { label = "high" }

print(label)
```

Using `when` you can chain as many boolean cases together as you want but if you
have more than one you need to wrap them in curly braces.

```Moo
when
{
    <case_one> : <statement_one>
    <case_two> : <statement_two>
    else : <else_statement>
}
```

These conditional statements are evaluated in the order that they are listed in
the `when` and only one of the statements is executed.

```Moo
var grade : string
let score = 75
when
{
    score >= 90 : grade = "A"
    score >= 80 : grade = "B"
    score >= 70 : grade = "C"
    score >= 60 : grade = "D"
    else : grade = "F"
}
print(grade)
```

`when` can also take a value to compare in the cases. This is passed into
parenthesis that directly follow the `when` keyword. When a value is passed in
the cases change from being boolean expression to any expression that produces a
value. Each case value is then compared against the passed in value and the
statement of the matching case is executed.

```Moo
let level = 6
when (level) {
    0 : print("Noob")
    1 : print("Fledgling")
    2 : print("Novice")
    3 : print("Wizard")
    4 : print("Archmage")
    5 : print("Demigod")
    else : print("Dead")
}
```

## While
`while` statements behave just as you would expect from any other language. The
`while` keyword is followed by parentheses containing a boolean expression.
Those parentheses are followed by a statement that is executed as long as the
boolean expression evaluates to true.

```Moo
while (<boolean_expression>) <statement>
```

A concrete example would be using a while statement to count to 5.

```Moo
var count = 0
while (count < 5)
{
    count++
    print("\(count)\n")
}
```

<sub>*Output*</sub>
```
1
2
3
4
5
```

## For
`for` loops in Moo follow the for-in syntax. The `for` keyword is followed by
parentheses that contain a variable and a collection separated by the keyword
`in`. The parentheses are followed by a statement that is called with each value
from the collection.

```Moo
let students = ["Alice", "Bob", "Carol", "David",]
for (name in students)
{
    print(name + "\n");
}
```

<sub>*Output*</sub>
```
Alice
Bob
Carol
David
```

If you need the index inside the loop you can just add a variable to hold it
inside of the parenthesis.

```Moo
for (name, index in students)
{
    print("\(index). \(name)\n")
}
```

<sub>*Output*</sub>
```
1. Alice
2. Bob
3. Carol
4. David
```

To use Moo's `for` loops like a legacy `for` loop you can use a range as the
collection.

```Moo
for (i in [0..<5])
{
    print("\(i), ")
}

```

<sub>*Output*</sub>
```
0, 1, 2, 3, 4,  
```
