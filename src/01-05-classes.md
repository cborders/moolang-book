# Classes
Classes are declared using the `class` keyword followed by the name of the
class. Delcaring member variables and methods follow the same syntax as anywhere
else exept they are scoped to within the class.

```Moo
class Shape
{
    var sides = 4

    fun stringify() : string
    {
        return "A shape with \(sides) sides"
    }
}
```

Classes have a special method called init that is called when a new instace of
that class is created. This is where you will put any code that you need to
initialize the class.

```Moo
class Shape
{
    var sides : int32

    init(sideCount : int32)
    {
        sides = sideCount
    }

    fun stringify() : string
    {
        return "A shape with \(sides) sides"
    }
}

let square = Shape(4)
```

## Access Modifers
Access to class' members is dictated using access modifiers. Access modifiers
are stateful and will stay in effect until explicitly changed or the end of the
class is reached. Access levels are declared using the `@public`, `@protected`,
and `@private` keywords.

|   Modifier   | Description |
|--------------|-------------|
| `@public`    | Public members are accessible anywhere |
| `@protected` | Protected members are accessible anywhere in the same file they are declared as well as in subclasses whether they are in the same file or not |
| `@private`   | Private members are only accessible within the class they are defined |

Private is the default access level of class class members. Updating the
previous example with access modifiers so that the stringify method is
accessible to other classes would look like this:

```Moo
class Shape
{
    var sides : int32

@public
    init(sideCount : int32)
    {
        sides = sideCount
    }

    fun stringify() : string
    {
        return "A shape with \(sides) sides"
    }
}

let square = Shape(4)
print(square.stringify())
```

<sub>*Output*</sub>
```
A shape with 4 sides
```
