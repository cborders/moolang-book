# Getting Started with Moo
This is built using [mdbook](https://github.com/rust-lang-nursery/mdBook)

To run the book while editing use `mdbook serve`.
To build for deployment use `mdbook build`.

[Introduction](src/00-introduction.md)

- [Quick Start Guide](src/01-quick-start.md)
    - [Basics](src/01-01-basics.md)
    - [Collections](src/01-02-collections.md)
    - [Control Flow](src/01-03-control-flow.md)
    - [Functions](src/01-04-functions.md)
    - [Classes](src/01-05-classes.md)
    - [Scope](src/01-06-scope.md)
- [Build System](src/02-build-system.md)
- [Appendix](src/appendix.md)
    - [Data Types](src/appendix-data-types.md)
    - [Standard Library](src/appendix-standard-library.md)
        - [List](src/std-lib-list.md)
        - [Map](src/std-lib-map.md)
        - [String](src/std-lib-string.md)
        - [Compiler](src/std-lib-compiler.md)
        - [IO](src/std-lib-io.md)
        - [Math](src/std-lib-math.md)
        - [Crypto](src/std-lib-crypto.md)